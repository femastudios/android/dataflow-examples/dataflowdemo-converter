package com.femastudios.android.dataflowdemo.converter.units

import com.femastudios.android.dataflowdemo.converter.Unit
import com.femastudios.android.dataflowdemo.converter.UnitType
import java.math.BigDecimal

object Mass : UnitType("Mass") {

    val GRAM = Unit(this, "Gram", BigDecimal.ONE)
    val KILOGRAM = Unit(this, "Kilogram", BigDecimal.ONE.movePointRight(3))
    val OUNCES = Unit(this, "Ounces", BigDecimal("28.349523125"))
    val POUNDS = Unit(this, "Pounds", BigDecimal("453.59237"))

    override val baseUnit = GRAM
    override val defaultStartUnit = GRAM
    override val defaultEndUnit = OUNCES

    override val units = listOf(
        GRAM,
        KILOGRAM,
        OUNCES,
        POUNDS
    )
}