package com.femastudios.android.dataflowdemo.converter

import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

/**
 * A unit (e.g. meter, second, etc.)
 */
data class Unit(
    /** The type of this unit */
    val type : UnitType,
    /** The name of this unit */
    val name: String,
    /** Function that converts this unit to the base one */
    val toBaseUnit : (BigDecimal) -> BigDecimal,
    /** Function that converts a base unit to this one */
    val fromBaseUnit : (BigDecimal) -> BigDecimal
) {
    /**
     * Constructor that accepts a ratio relative to the base unit
     *
     * @param baseRatio The ratio relative to the [UnitType.baseUnit]
     */
    constructor(type: UnitType, name: String, baseRatio: BigDecimal) : this(type, name, {
        it.multiply(baseRatio, MathContext(100, RoundingMode.HALF_UP))
    },{
        it.divide(baseRatio, MathContext(100, RoundingMode.HALF_UP))
    })

    override fun toString() = name

    /**
     * Converts the given [value] to the given [unit]
     */
    fun convert(value: BigDecimal, unit: Unit): BigDecimal {
        return unit.fromBaseUnit(toBaseUnit(value))

    }
}