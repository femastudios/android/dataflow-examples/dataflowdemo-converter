package com.femastudios.android.dataflowdemo.converter.units

import com.femastudios.android.dataflowdemo.converter.Unit
import com.femastudios.android.dataflowdemo.converter.UnitType
import java.math.BigDecimal

object Temperature : UnitType("Temperature") {

    private val C_TO_K = BigDecimal("273.15")

    val KELVINS = Unit(this, "Kelvins", BigDecimal.ONE)
    val CELSIUSES = Unit(this, "Celsiuses", { it + C_TO_K }, { it - C_TO_K })
    val FAHRENHEITS = Unit(this, "Fahrenheits", {
        (it - 32.toBigDecimal()) * (5 / 9f).toBigDecimal() + C_TO_K
    }, {
        (it - C_TO_K) * (9 / 5f).toBigDecimal() + C_TO_K
    })

    override val baseUnit = KELVINS
    override val defaultStartUnit = CELSIUSES
    override val defaultEndUnit = FAHRENHEITS

    override val units = listOf(
        KELVINS,
        CELSIUSES,
        FAHRENHEITS
    )
}