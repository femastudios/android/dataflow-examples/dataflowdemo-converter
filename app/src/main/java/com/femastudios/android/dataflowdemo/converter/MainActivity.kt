package com.femastudios.android.dataflowdemo.converter

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.femastudios.android.core.context
import com.femastudios.android.core.dp
import com.femastudios.android.dataflowdemo.converter.units.Distance
import com.femastudios.dataflow.Field
import com.femastudios.dataflow.MutableField
import com.femastudios.dataflow.android.declarativeui.*
import com.femastudios.dataflow.android.viewState.extensions.setBackgroundColor
import com.femastudios.dataflow.android.viewState.states.padding
import com.femastudios.dataflow.extensions.orEmpty
import com.femastudios.dataflow.extensions.plus
import com.femastudios.dataflow.extensions.transform
import com.femastudios.dataflow.util.fieldOf
import com.femastudios.dataflow.util.mutableFieldOf
import com.femastudios.dataflow.util.transform
import java.math.BigDecimal
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    companion object {
        /** Object used to format the decimal values before displaying them */
        private val DECIMAL_FORMAT = DecimalFormat().apply {
            minimumIntegerDigits = 1
            minimumFractionDigits = 0
            maximumFractionDigits = 6
            decimalFormatSymbols.apply {
                decimalSeparator = '.'
                groupingSeparator = ' '
            }
        }
    }

    //FIELDS
    //Here we can find fields that are our source of truth for this app.
    //Three of them can be changed by the user, while the last one is simply a transformation of the other three.

    /** MutableField that contains the value to convert. Can be `null` because the user might enter an incorrect value in (e.g. leave the edit text empty) */
    private val numberA = mutableFieldOf<BigDecimal?>(BigDecimal.ONE)
    /** MutableField that contains the unit of the first number */
    private val numberAUnit = mutableFieldOf(Distance.baseUnit)

    /** MutableField that contains the unit of the second number */
    private val numberBUnit = mutableFieldOf(Distance.defaultEndUnit)
    /** Field that, given the first number and the two units, converts the value. If numberA is null, this will also be null */
    private val numberB = transform(numberA, numberAUnit, numberBUnit) { number, unitA, unitB ->
        if (number == null) null else unitA.convert(number, unitB)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //In the on create we create the views, using the declarative UI library
        setContentView(context.new.linearLayout(vertical = true) {
            add {
                height = 0
                weight = 1F
            }.linearLayout {
                //Add selector to change the desired UnitType
                addSelectableRecyclerView(
                    "Unit type",
                    fieldOf(ALL_TYPES),
                    { it.name.hashCode().toLong() },
                    { ut -> numberAUnit.transform { it.type == ut } },
                    { v, ut ->
                        v.text = ut.name
                        v.setOnClickListener {
                            numberAUnit.value = ut.defaultStartUnit
                            numberBUnit.value = ut.defaultEndUnit
                        }
                    }
                )
            }

            //Adds two selectors that allow to change the from/to units
            add {
                height = 0
                weight = 1F
            }.linearLayout {
                addUnitsRecyclerView("Convert from", numberAUnit)
                addUnitsRecyclerView("Convert to", numberBUnit)
            }


            //Adds the edit text that allows to change the data and the result text views
            add.linearLayout(vertical = true) {
                add.editText(numberA.twoWayTransform(
                    { it?.toString() ?: "" },
                    { it.toBigDecimalOrNull() }
                )) {
                    textSize = 64f
                    gravity = Gravity.CENTER
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                }
                add.text(numberAUnit.toStringF() + "\n=") {
                    textSize = 28f
                    gravity = Gravity.CENTER
                    setTextColor(Color.BLACK)
                }
                add.text(numberB.transform {
                    if (it == null) null
                    else DECIMAL_FORMAT.format(it)
                }.orEmpty()) {
                    textSize = 64f
                    gravity = Gravity.CENTER
                    setTextColor(Color.BLACK)
                }

                add.text(numberBUnit.toStringF()) {
                    textSize = 28f
                    gravity = Gravity.CENTER
                    setTextColor(Color.BLACK)
                }
            }

        })
    }

    /**
     * Adds a [RecyclerView] to the receiver [LinearLayout] that contains the given [Unit]s.
     *
     * @see addSelectableRecyclerView
     */
    private fun LinearLayout.addUnitsRecyclerView(title: String, unitField: MutableField<Unit>) {
        addSelectableRecyclerView(
            title,
            unitField.transform { it.type.units },
            { it.name.hashCode().toLong() },
            { u -> unitField.transform { it == u } },
            { v, u ->
                v.text = u.name
                v.setOnClickListener {
                    unitField.value = u
                }
            }
        )
    }

    /**
     * Adds a [RecyclerView] to the receiver [LinearLayout], with some additional things, useful for this app:
     * * Wraps it in another [LinearLayout], adding the provided [title] before it
     * * Defines a view creator that creates [TextView]s, with a certain style
     * * Automatically binds the background color of the text views to match the selected state given by [itemSelected]
     *
     * @see ViewCreator.recyclerView
     */
    private inline fun <T> LinearLayout.addSelectableRecyclerView(
        title: String,
        items: Field<List<T>>,
        noinline idProvider: (T) -> Long,
        crossinline itemSelected: (T) -> Field<Boolean>,
        crossinline viewBinder: (TextView, T) -> kotlin.Unit
    ) {
        add {
            matchHeight()
            width = 0
            weight = 1F
        }.linearLayout(vertical = true) {
            add.text(title) {
                textSize = 28f
                setTextColor(Color.BLACK)
            }
            add {
                matchWidth()
                matchHeight()
            }.recyclerView(
                items,
                idProvider,
                {
                    new.text {
                        padding(dp(16))
                        textSize = 20f
                    }
                },
                { v, i ->
                    v.setBackgroundColor(
                        itemSelected(i).transform(
                            0xFFB3E5FC.toInt(),
                            Color.TRANSPARENT
                        )
                    )
                    viewBinder(v, i)
                }
            )
        }
    }
}
