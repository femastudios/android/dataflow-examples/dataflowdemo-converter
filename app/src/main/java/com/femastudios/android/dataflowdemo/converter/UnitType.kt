package com.femastudios.android.dataflowdemo.converter

import com.femastudios.android.dataflowdemo.converter.units.Distance
import com.femastudios.android.dataflowdemo.converter.units.Mass
import com.femastudios.android.dataflowdemo.converter.units.Temperature
import com.femastudios.android.dataflowdemo.converter.units.Time

/**
 * Represents type of unit (e.g. time, length, mass, etc.)
 */
abstract class UnitType(
    /** Name of the unit type */
    val name: String
) {
    /** List of units belonging to this unit type */
    abstract val units: List<Unit>
    /** The base unit (e.g. meter for length) */
    abstract val baseUnit : Unit
    /** The unit to put as "convert from" when selecting this unit type. */
    abstract val defaultStartUnit : Unit
    /** The unit to put as "convert to" when selecting this unit type. */
    abstract val defaultEndUnit : Unit
}


/** List of all unit types */
val ALL_TYPES = listOf(Distance, Mass, Temperature, Time)