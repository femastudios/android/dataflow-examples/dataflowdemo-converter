package com.femastudios.android.dataflowdemo.converter.units

import com.femastudios.android.dataflowdemo.converter.Unit
import com.femastudios.android.dataflowdemo.converter.UnitType
import java.math.BigDecimal

object Time : UnitType("Time") {

    val SECONDS = Unit(this, "Seconds", BigDecimal.ONE)
    val MINUTES = Unit(this, "Minutes", BigDecimal("60"))
    val HOURS = Unit(this, "Hours", BigDecimal("3600"))
    val DAYS = Unit(this, "Days", BigDecimal("86400"))

    override val baseUnit = SECONDS
    override val defaultStartUnit = MINUTES
    override val defaultEndUnit = SECONDS

    override val units = listOf(
        SECONDS,
        MINUTES,
        HOURS,
        DAYS
    )
}