package com.femastudios.android.dataflowdemo.converter.units

import com.femastudios.android.dataflowdemo.converter.Unit
import com.femastudios.android.dataflowdemo.converter.UnitType
import java.math.BigDecimal

object Distance : UnitType("Distance") {

    val MILLIMETERS = Unit(this, "Millimeters", BigDecimal.ONE.movePointRight(-3))
    val CENTIMETERS = Unit(this, "Centimeters", BigDecimal.ONE.movePointRight(-2))
    val METERS = Unit(this, "Meters", BigDecimal.ONE)
    val KILOMETERS = Unit(this, "Kilometers", BigDecimal.ONE.movePointRight(3))
    val MILES = Unit(this, "Miles", BigDecimal("1609.344"))
    val INCHES = Unit(this, "Inches", BigDecimal("0.0254"))
    val YARDS = Unit(this, "Yards", BigDecimal("0.9144"))
    val FEET = Unit(this, "Feet", BigDecimal("0.3048"))

    override val baseUnit = METERS
    override val defaultStartUnit = METERS
    override val defaultEndUnit = FEET

    override val units: List<Unit> = listOf(
        MILLIMETERS,
        CENTIMETERS,
        METERS,
        KILOMETERS,
        MILES,
        INCHES,
        YARDS,
        FEET
    )


}