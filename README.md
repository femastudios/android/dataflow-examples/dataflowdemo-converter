# Converter
This app is a simple converter of units that demonstrates a basic usage of fields.

In particular, at the top of [`MainActivity`](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-converter/blob/master/app/src/main/java/com/femastudios/android/dataflowdemo/converter/MainActivity.kt) we have three fields whose value can be controlled by the user. Then we have a read-only field that tranforms them and calculates our result. A few other transformations are performed where needed in the activity code.

The app also uses the `declarativeui` library to create its views. 

You read the comments in the code for more details.


## Screenshot
![Screenshot](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-converter/raw/master/screenshot.png)

## Install
You can go to [releases](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-converter/-/releases) to find an APK to install.